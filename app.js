var express = require('express');
var app = express();
app.get('/secret', function (req, res) {
  res.send(process.env.HELLO_KUBERNETES_SECRET);
});
app.get('/config', function (req, res) {
  res.send(process.env.HELLO_KUBERNETES_CONFIG);
});
app.get('/', function (req, res) {
  res.send('Hello Kubernetes!');
});
app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
